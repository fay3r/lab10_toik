package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public Boolean checkLogin(final String login, final String password) {
        for (int index = 1; index < usersDatabase.size(); index++) {
            if (usersDatabase.get(index).getLogin().equals(login)) {
                if (usersDatabase.get(index).isActive()) {
                    if (usersDatabase.get(index).getIncorrectLoginCounter() != 3) {
                        if (usersDatabase.get(index).getPassword().equals(password)) {
                            return true;
                        }
                    }
                }
                if (usersDatabase.get(index).getIncorrectLoginCounter() == 3) {
                    usersDatabase.get(index).deactivateAccount();
                    return null;
                }
                usersDatabase.get(index).incrementIncorrectLoginCounter();
            }
        }
        return false;

    }
}
